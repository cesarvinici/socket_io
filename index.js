var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var fs = require('fs');

var index = fs.readFileSync(__dirname+ '/index.html');




app.get('/', function(req, res){
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});

io.on('connection', function(socket){
    //console.log('new conecction '+socket.id);
    socket.on('msg', function(msg){
        console.log(msg);
        socket.broadcast.emit('user connected', 'User '+socket.id+" connected!");
    });
    

    socket.on('disconnect', function(){
        socket.broadcast.emit('user off', '0');
    });

    socket.on('chat message', function(msg){
        console.log('message: '+msg);
    });

    socket.on('chat message', function(msg){
        io.emit('chat message', msg);
    });

    socket.on('digitando', function(msg){
        console.log(msg);
        socket.broadcast.emit('digitando', msg);
    })


});

http.listen(3000, function(){
    console.log("Listening on *:3000");
});